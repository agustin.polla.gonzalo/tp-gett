import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/interfaces/Task';
import { TaskService } from 'src/app/services/TaskService';
import { NgxPaginationModule } from 'ngx-pagination';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html'
})

export class TaskListComponent implements OnInit {
  // Deberia ser: tasks: Task[] | undefined;
  // Por el plugin es:
  tasks!: any[];
  page: number = 1;
  perPage = 5;

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.taskService.getList().subscribe(tasks => {
      this.tasks = tasks;
    });
  }

  delete(task: Task) {
    this.taskService.delete(task).subscribe(() => {
      this.getTasks();
    });
  }

  // Requerido porque el plugin "pierde el tipo" al paginar
  asTask(task: any) {
    return (task as Task);
  }
}
