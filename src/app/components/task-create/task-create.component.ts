import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Task } from 'src/app/interfaces/Task';
import { TaskService } from 'src/app/services/TaskService';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html'
})
export class TaskCreateComponent implements OnInit {

  createTaskForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private taskService: TaskService
  ) { }

  ngOnInit() {
    this.createTaskForm = this.formBuilder.group({
      title: ['', Validators.required],
      completed: [false, Validators.required]
    });
  }

  onSubmit() {
    const task = this.createTaskForm?.value as Task;
    this.taskService.add(task)
      .subscribe(() => {
        this.router.navigate(['/tasks']);
      });
  }

}
