import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Task } from 'src/app/interfaces/Task';
import { TaskService } from 'src/app/services/TaskService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html'
})
export class TaskEditComponent implements OnInit {

  editTaskForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private taskService: TaskService
  ) { }

  ngOnInit() {
    this.editTaskForm = this.formBuilder.group({
      id: ['', Validators.required],
      title: ['', Validators.required],
      completed: [false, Validators.required]
    });
    this.getTask();
  }

  getTask() {
    const id = +(this.route.snapshot.paramMap.get('id') || 0);
    this.taskService.getOne(id)
      .subscribe(task => {
        if (!task) {
          this.router.navigate(['/tasks']);
        };
        this.editTaskForm.patchValue(task);
      });
  }

  onSubmit() {
    const task = this.editTaskForm?.value as Task;
    this.taskService.update(task)
      .subscribe(() => {
        console.log(task);
        this.router.navigate(['/tasks']);
      });
  }

}
