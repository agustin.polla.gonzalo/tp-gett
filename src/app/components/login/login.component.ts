import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/AuthenticationService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  invalidLogin = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.authenticationService.login(this.username, this.password)) {
      this.router.navigate(['/tasks']);
      this.invalidLogin = false;
    } else {
      this.invalidLogin = true;
    }
  }
}
