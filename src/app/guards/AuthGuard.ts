import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const hardcodedUser = 'admin';

    const user = localStorage.getItem('loggedInUser');

    if (user === hardcodedUser) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
