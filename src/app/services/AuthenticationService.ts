import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private hardcodedCredentials = {
    username: 'admin',
    password: 'admin'
  };

  constructor(private router: Router) { }

  login(username: string, password: string) {
    if (username === this.hardcodedCredentials.username && password === this.hardcodedCredentials.password) {
      localStorage.setItem('loggedInUser', username);
      this.router.navigate(['/tasks']);
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('loggedInUser');
    this.router.navigate(['/']);
  }

  isUserLoggedIn() {
    return localStorage.getItem('loggedInUser') !== null;
  }

  getLoggedInUser() {
    return localStorage.getItem('loggedInUser');
  }
}
