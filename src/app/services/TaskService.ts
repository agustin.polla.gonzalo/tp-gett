import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Task } from '../interfaces/Task';

@Injectable({
  providedIn: 'root'
})

export class TaskService {
  private API_URL = 'https://jsonplaceholder.typicode.com/todos';
  private tasks: Task[] = [];

  /**
   * Se precarga la lista de tareas para poder operar.
   *
   * @param http Http Injection
   */
  constructor(private http: HttpClient) {

  }

  getList(): Observable<Task[]> {
    if (this.tasks.length === 0) {
      return this.http.get<Task[]>(this.API_URL)
        .pipe(
          tap(tasks => this.tasks = tasks)
        );
    } else {
      return of(this.tasks);
    }
  }


  getOne(id: number): Observable<Task> {
    return of(
      this.tasks.filter(originalTask => {
        return originalTask.id === id;
      })[0] ?? null
    );
  }

  add(task: Task): Observable<Task> {
    this.tasks.push(task);
    return of(task);
  }

  update(task: Task): Observable<Task> {
    this.tasks = this.tasks.map(originalTask => {
      if (originalTask.id === task.id) {
        return { ...originalTask, ...task };
      }
      return originalTask;
    });

    return of(task);
  }

  delete(task: Task): Observable<Task> {
    this.tasks = this.tasks.filter(originalTask => originalTask.id !== task.id);
    return of(task);
  }
}
